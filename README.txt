
Webform Bagatelles
==================
bagatelle |ˌbagəˈtɛl| noun:
"something regarded as too unimportant to be worth much consideration"

Webform Bagatelles currently adds two functions to Webform.

1) On the Webform -> Form Settings tab (node/#/webform/configure), in the
bottom fieldset labeled Advanced Settings there's a new radio button selection
to control where the Submit/Prev/Next buttons should go on each page of a
single or multi-step form. By default Webform puts these buttons at the very
bottom of the form, regardless of the way you may wish to arrange your form
elements.
By selecting "Immediately below the last form element that submits data", you
give yourself the flexibility to arrange containers and markup components around
the form, before or after the actual data submission elements, while keeping the
buttons immediately below these at all times.
All this works particularly well in combination with the Webform Layout module,
http://drupal.org/project/webform_layout.

2) The module exposes in the session the page number and page count of the
webform a visitor is currently on. This information can thus be picked up by
any module or page element to do something with.

For instance a block could become visible above certain Webform page number with
the code snippet below, entered in the text area "Pages on which this PHP code
returns TRUE":

   <?php return webform_bagatelles_page_num_is_above(2); ?>

To disambiguate a webform from others or to target a Webform that is in a block
you can add the webform's form ID as a second argument:

   <?php return webform_bagatelles_page_num_is_above(2, 'webform_client_form_32');

Note: for the option "Pages on which this PHP code returns TRUE" to appear on
the block configuration page, you need to enable the core module "PHP filter".

To facilitate fine-grained styling, Webform Bagatelles also adds a handy page
identifier to the form's CSS classes.

    <form class="webform-client-form webform-client-form-32 webform-client-form-page-2" ...


By RdB
for BN, New Year's Day 2015
for TH, Valentine's Day 2015